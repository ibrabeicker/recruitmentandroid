package com.recruitment.api;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.recruitment.R;
import com.recruitment.api.models.CategoriesResponse;
import com.recruitment.api.models.NobelPrizes;
import com.recruitment.api.models.Prize;
import com.recruitment.api.models.YearsResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by ibrahim on 20/02/17.
 */

public class RestApi {

    private static NobelPrizes prizes;

    private static Gson gson = new Gson();

    public static void initPrizes(Context context) {
        if (prizes == null) {
            InputStream is = context.getResources().openRawResource(R.raw.prize);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (IOException e) {
                Log.e("TAG", e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }

            String jsonString = writer.toString();
            Gson gson = new Gson();
            prizes = gson.fromJson(jsonString, NobelPrizes.class);
        }
    }

    public static void listYears(final Context context, final ResponseCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                initPrizes(context);
                HashSet<String> yearSet = new HashSet<>();
                for (Prize p : prizes.prizes) {
                    yearSet.add(p.year);
                }
                ArrayList<String> yearsList = new ArrayList<>(yearSet);
                Collections.sort(yearsList);
                Collections.reverse(yearsList);
                String response = gson.toJson(new YearsResponse(yearsList));
                callback.onResponse(response);
            }
        }).start();
    }

    public static void listCategories(final Context context, final ResponseCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                initPrizes(context);
                HashSet<String> categorySet = new HashSet<>();
                for (Prize p : prizes.prizes) {
                    categorySet.add(p.category);
                }
                ArrayList<String> categories = new ArrayList<>(categorySet);
                Collections.sort(categories);
                String response = gson.toJson(new CategoriesResponse(categories));
                callback.onResponse(response);
            }
        }).start();
    }

    public static void listLaureates(final Context context, final String year, final String category, final ResponseCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                initPrizes(context);
                for (Prize p : prizes.prizes) {
                    if (p.category.equals(category) && p.year.equals(year)) {
                        String response = gson.toJson(p);
                        callback.onResponse(response);
                    }
                }
                callback.onResponse("{}");
            }
        }).start();
    }
}
