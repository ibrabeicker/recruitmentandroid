package com.recruitment.api;

/**
 * Created by ibrahim on 20/02/17.
 */

public interface ResponseCallback {

    void onResponse(String response);
}
