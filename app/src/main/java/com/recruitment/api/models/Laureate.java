package com.recruitment.api.models;

/**
 * Created by ibrahim on 20/02/17.
 */

public class Laureate {

    public String id;
    public String firstname;
    public String surname;
    public String motivation;
    public String share;

    public Laureate() {

    }
}
