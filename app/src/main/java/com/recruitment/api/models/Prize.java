package com.recruitment.api.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ibrahim on 20/02/17.
 */

public class Prize {

    public String year;
    public String category;
    public List<Laureate> laureates = new ArrayList<>();

    public Prize() {

    }
}
