package com.recruitment.api.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ibrahim on 20/02/17.
 */

public class CategoriesResponse {

    public List<String> categories = new ArrayList<>();

    public CategoriesResponse() {

    }

    public CategoriesResponse(List<String> categories) {
        this.categories = categories;
    }
}
