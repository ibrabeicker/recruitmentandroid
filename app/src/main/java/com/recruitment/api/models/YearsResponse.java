package com.recruitment.api.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ibrahim on 20/02/17.
 */

public class YearsResponse {

    public List<String> years = new ArrayList<>();

    public YearsResponse() {

    }

    public YearsResponse(List<String> years) {
        this.years = years;
    }
}
